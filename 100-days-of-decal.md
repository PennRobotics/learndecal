One Hundred Days of DECAL
=========================

[[_TOC_]]


-----

## Days 1 to 10

### Day 1: (10 June 2024) Familiarize with C\# and .NET

There is a general overview on HackerRank for how they certify C#.

To begin, C# was developed by Microsoft as a scripting language. It gets converted by the Common Language Runtime (CLR), which is generally .NET. In contrast to C++, C# has garbage collection. However, _any_ collection is better than _no_ collection, so we forgive them for that foible. C# is supposedly _more object oriented_ (hooray!) than C++ and also has no complex functionality, so it is easy to learn and implement. (Maybe Microsoft also claims a good chef can just hop right over to pastry baking.)

One critical element for Decal is going to be _Interfaces_. C# will not do multiple inheritance by its class construction; rather, interfaces are used to create interdependency with other modules. Since Decal is a large collection of interfaces that make server and client messages and data available to a developer, it will be important to know how an interface works and which are made available via Decal (and, likely, the Virindi plugin ecosystem).

A minor detail: CLR is also accessible using VB.NET (essentially Visual BASIC with some fluff) and F#.

Without getting into the naming history of .NET, I'll try to focus on a modern (version 8 or 9) .NET and then revert to an older version if I think it's necessary. (A decent number of Decal plugins use .NET 2.0 or earlier. For me, this entails correctly configuring Wine, installing all of the developer tools, backing up this entire directory tree, and hoping I have all of the proper dependencies. It is akin to Docker except&mdash;astonishingly, as hard as it might be to believe&mdash;shittier than Docker.)

#### C\# Study Plan

- general structure, namespaces, types, values, references
- nullable vs non-
- OOP: classes and objects, inheritance, interfaces
- properties and indexers
- collections (standard library generics: list, set, dict, &hellip;)
- exception handling: try-catch-finally
- generics
- lambdas, delegates
- iterators
- LINQ for interacting with IEnumerable collections
- resource mgmt (incl. _IDisposable_)
- anonymous/dynamic types
- async, await, locks, _ThreadPool_
- reflection (type introspection)
- interfacing with unmanaged code (Native Interop, pointers, best practices)
- expression trees for application DSLs

#### .NET Study Plan

- types, classes, structures, enums, interfaces, delegates
- I/O: files, network, HTTP
- data manipulation: libraries for strings, dates, times, machine readable formats
- collections
- LINQ
- routing (directing requests based on URL)
- memory cache; defaults and limits
- REST API
- data access &amp; database API
- .NET Core versus .NET Framework versus .NET Standard; specifically, programming APIs
- Common Language Infrastructure; Roslyn compiler API and .NET-compliant runtimes
- assemblies: file formats, metadata, linking modules
- globalization, localization
- async, tasks
- parallel programming: Task Parallel Library and ParallelLINQ
- attributes
- security: roles, best code practices, crypto
- memory management
- unsafe code, incl. .DLL and .COM library interactions

#### Progress

In trying to submit the _basic_ certification test for .NET, I see that a significant amount of my time was spent porting SQL syntax to LINQ and then getting the appropriate type conversion. Specifically, I needed to convert from an _IEnumerable_ to a dictionary (where the value is a custom type) and kept encountering different errors, although I was seemingly able to figure most of it out within the hour provided, as I received a certificate.

One major help was finding [Microsoft examples](https://github.com/dotnet/try-samples/blob/main/101-linq-samples/src/AggregateOperators.cs).

Using an online test platform made much of the work easier, since there is a known test case and the compiler is already correctly configured. I think going forward, I would try to keep these submissions (or something similar) to make sure that my local installation is set up correctly.

Day 1? Acceptable.


### Day 2: (14 June 2024) Familiarize with C\# and .NET, continued

I solved _TwoSum_ using C# on LeetCode. The solution was suboptimal. In Python, if the input is large enough (n > 1000) I would enumerate each input number (tacking this value as the second value in a pair with the original number as the first value) and then sort the input numbers. Then, binary search the second number for each first number. For a good time, use several threads so four or eight first numbers at a time can be checked. This, however, would only be worth it for very large arrays (n > 100,000 in my opinion) The tough part, because I don't know .NET and multithreading so well, would be stopping the other threads early after any thread finds a valid result.

Day 2: good enough. I learned sort needs `Array.Sort(arr)` although `arr.Reverse()` works, plus debug lines use `System.Console.WriteLine(ln)` LeetCode supports C# but does not have a .NET study track.


<mark> TODO: days 6 to 8 should be "Install a .NET IDE and compile tools" </mark>

<mark> TODO: days 9 and 10? "Create a basic library that outputs to chat window on startup" </mark>

-----

<mark> Days 11 to 20 establish basic Decal elements, including GUI creation </mark>

<mark> Days 21 to 65 should discover all of Decal and .NET functionality </mark>

<mark> Days 66 to 75 should ensure I can test and compile properly </mark>

At this point, two things should happen: (1) the entirety of Decal development is documented, and (2) a Docker-like system of compiling is constructed. The second step would mean my computer does not need any software installed to generate a library. This means documentation can be written which anyone can follow. (The key will be making sure that any log messages related to bad compilation are shown clearly and quickly. The biggest issue I had with GitHub Actions was waiting 2+ minutes for feedback on any error, large or small. Miss a parenthesis, forget to press shift in a variable name, use a wrong datatype or member, forget a logging output, and before you know it two hours are gone and you still have not compiled successfully!) Maybe I could also document the "rapid development" case, where everything _IS_ installed locally&mdash;step-by-step and quite detailed&mdash;so that errors can be quickly crushed before uploading to the cloud compiler. This, I expect, might take far longer than ten days. The assumption, at this point, is that I still magically have maintained enough interest and motivation to see this through to completion.

<mark> Days 76 to 100 should be attempts at plugin development </mark>