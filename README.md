# Learn Decal Programming

_instructions for democratizing old-ass game extensions_


-----
<h2><mark>"IN PROGRESS" SECTION</mark></h2>

### Checklist

- [ ] Make a .NET project that works with some of the Decal libraries
    - [ ] What needs to be installed via NuGet?
- [ ] Run `dotnet` commands via [CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] Save necessary information (e.g. protocol details, icon charts, API) locally
- [ ] [SAST](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] Badges? &hellip; _Badges??_ &hellip;
- [ ] Screenshots
- [ ] Use _DebugView_ (from SysInternals) to write debug statements
- [ ] Steps for creating a plugin installer
    - [ ] Advanced: manually adding a plugin using Wine registry, etc.

### 100 Days

<mark>While progressing, remember to document everything in the other markdown files</mark>

- [ ] 1 &ndash; 5, Familiarize with C# and .NET
- [ ] 6 &ndash; 8, Install a .NET IDE and compile tools
- [ ] 9 &ndash; 10, Create a basic library that outputs to chat window on startup
- [ ] 11 &ndash; 15, Create a view window and experiment with layout styles
- [ ] 16 &ndash; 20, Use each of the widget types
- [ ] 21 &ndash; 25, Timers and state machines
- [ ] 26 &ndash; 30, Input fields and validation
- [ ] 31 &ndash; 35, Decal messages
- [ ] 36 &ndash; 40, Local file access, storage, config
- [ ] 41 &ndash; 45, Network calls
- [ ] 46 &ndash; 50, Notification methods
- [ ] 51 &ndash; 55, Database access
- [ ] 56 &ndash; 60, Threading?
- [ ] 61 &ndash; 65, Fonts, icons, custom visuals
- [ ] 66 &ndash; 70, Testing
- [ ] 71 &ndash; 75, Compile open source projects
- [ ] 76 &ndash; 80, Develop: an outdated but useful tool (e.g. Target Info)
- [ ] 81 &ndash; 85, Develop: a "report missing location" tool
- [ ] 86 &ndash; 90, Develop: a "report broken quest/item/NPC" tool
- [ ] 91 &ndash; 95, Develop: spellcast success likelihood display
- [ ] 96 &ndash; 100, Develop: most recent skill calcs if missing, or something else

### Living instructions

```sh
mkdir dotnet-test
cd dotnet-test
dotnet new classlib
dotnet build
# at this point, a bin/ directory should be nonempty
dotnet clean

# TODO: here, the necessary .dlls from Decal and/or Virindi should be placed in the appropriate path---whatever that may be.

# now, the various source files should be edited
vi Class1.cs dotnet-test.csproj
```

### C#

```cs
using System;
using System.Collections.Generic;
using System.Text;

using Decal.Adapter;

namespace WhateverNameHere
{
    public class PluginCore : PluginBase
    {
        private void CharacterFilter_LoginComplete(object sender, EventArgs e)
        {
            Host.Actions.AddChatText("plugin ready for development", 1);
        }             

        protected override void Startup()
        {
            Core.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
        }

        protected override void Shutdown()
        {
            Core.CharacterFilter.LoginComplete -= CharacterFilter_LoginComplete;
        }
    }
}
```

### Track down these:

- PluginTemplate2.9.x.x (zip)
- HotDecalPluginProjectTemplate (zip)
- decaldevbook-master (zip)

<!-- end of "IN PROGRESS" SECTION -->
-----


## Getting started

This guide aims to expose the fundamentals necessary for writing a Decal plugin. Decal is an environment that lets various plugins&mdash;usually each in a .dll file&mdash;access the client data for Asheron's Call, a MMORPG which is no longer commercially hosted but has been revived thanks to the monumental efforts of server emulator authors.

It seems one will need to become familiar with .NET ("dot net"). It seems .NET grew into various standalone projects: the Windows-only _.NET Framework_, the open-source and cross-platform _.NET Core_, and a clean room implementation originally for Linux called either _Mono_ or _Xamarin_. These have been more recently quasi-merged into a single environment: the confusingly and uncreatively named _.NET_, currently at version 8.0.

A class library would be generated in a way that lets it interact with various Decal libraries (and potentially 3rd party libraries such as those under the _Virindi_ moniker).

It is important to note that this guide currently favors solutions that work with Wine in Linux.

Ideally, I would find a way to generate each library using a container. This might make development easier for cases where installing the .NET SDK is impractical or troublesome. However, I am a lightning rod for container bullshit&mdash;uncovering error messages related to IPv6, certificates, proxies, ports, mounts, credentials, environmental variables, version incompatibility, container folders and paths, compose, VM hypervisor, network bridge, USB, and more. It fascinates me that people "magic bullet" Docker when I often get it to fail simply by running some example/tutorial code step-by-step! Nonetheless, I will try getting GitLab to CI/CD a .NET container and automatically build .dll files for custom plugins.


### Working locally

```
git clone https://gitlab.com/PennRobotics/learndecal.git
cd learndecal
```
<mark>TODO</mark>: what next?


## Tools

- [.NET](https://dotnet.microsoft.com/download) using either C# or Visual Basic (VB)
- [Decal](https://www.decaldev.com/)
- [a good code editor](https://www.vim.org/download.php)
- _Asheron&rsquo;s Call_, for plugin testing and usage


## Support

If there are problems, add an issue and/or track down the "ACPluginsDev" Discord server.


## References

<mark>TODO</mark>


## Roadmap

- Using C# 9.0 for modern development
- Using .NET 4.8 or 4.0 or 2.0 for running on legacy platforms or quicker porting of existing source
- Reviving dead plugins
    - Target Info
        - can this be further improved to show the likely odds of defense, crits, etc??
    - others?? <mark>TODO</mark>


## Authors and acknowledgment

- @PennRobotics (Brian; _Archmage Cutz_ on both Coldeve and Levistras)


## License

MIT
