How a .csproj file might be configured:

http://www.virindi.net/repos/virindi_public/tags/VTClassic/1.0.1.0/VTClassic/VTClassic/VTClassic.csproj

Alternate choice:

https://github.com/amoeba/treestats/blob/master/Treestats.csproj

Yet another:

https://github.com/mudzereli/mudsort/blob/master/PluginCore.cs

-----

Most of the Decal DLLs have similar exports:

- DllCanUnloadNow
- DllGetClassObject
- (some) DllMain
- DllRegisterServer
- DllUnregisterServer

**Decal.dll** adds

- DispatchOnChatCommand

**DecalNet.dll** adds

- FeedPacketHandler

**Inject.dll** has a bit more:

- BeginSceneO
- Container_Draw
- Container_Initialize
- Container_StartPlugins
- Container_StopPlugins
- Container_Terminate
- DecalStartup
- DllCanUnloadNow
- DllGetClassObject
- DllRegisterServer
- DllUnregisterServer
- EndSceneO
- GetVTableEntry
- HookVTable
- InjectMapPath
- PatchMemory
- UnhookVTable

**LauncherHook.dll** _only_ has:

- LauncherHookDisable
- LauncherHookEnable
- LauncherHookLauncherStartup

> **NOTE**
> _NONE_ of the ".NET 2.0 PIA" .dll files have exports!

There are also:

- updatelist.xml
- messages.xml
- memlocs.xml is garbled!
- decalplugins.xml
- Decal.FileService.xml
- Decal.Adapter.xml (probably the best chance for useful info so far)
- clientpatches.xml